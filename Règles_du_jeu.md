# Règles du jeu

- **Nombre de joueurs** : 2 à 4
- **Type** : [jeu de cartes](https://www.regledujeu.fr/jeux-de-cartes/)
- **Durée** : 30 min
- **But du jeu** : Posséder le plus de PV (pts de victoire) en cumulant des *cartes Développement* et des *tuiles Nobles*. *Les PV sont indiqués en haut à gauche des cartes/tuiles*.

![https://www.regledujeu.fr/wp-content/uploads/splendor-pv-cout.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-pv-cout.jpg)

– Le cercle vert (partie supérieure gauche de la carte développement) indique le nombre de PV (points de victoire) que la carte vous octroie.– Les valeurs encadrées en bleu indiquent le coût de la carte en pierres précieuses.

- **Principe** : Acheter des *cartes Développement* à l’aide de vos *Jetons pierres précieuses*.
- **Fin de la partie** : Un joueur possède 15 PV (on termine le tour)
- **Jeu similaire :** [Century : la Route des Epices](https://www.regledujeu.fr/century-la-route-des-epices/)
- **Matériel** :

40 *jetons (**pierre précieuses**)*

![https://www.regledujeu.fr/wp-content/uploads/splendor-jetons.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-jetons.jpg)

90 ***cartes Développement***(niv1 : 40 cartes, niv2 : 30 cartes, niv3 : 20 cartes)

![https://www.regledujeu.fr/wp-content/uploads/splendor-developpement.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-developpement.jpg)

10 *tuiles **Noble***

![https://www.regledujeu.fr/wp-content/uploads/splendor-noble.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-noble.jpg)

---

---

# **I – Préparation**

### **I.a. Cartes Développement**

![https://www.regledujeu.fr/wp-content/uploads/splendor-cartes-developpement-64x150.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-cartes-developpement-64x150.jpg)

1. Poser les 3 paquets de *cartes Développement* par ordre de niveau (après avoir mélangé chaque paquet) :
    - Paquet du bas : niveau 1
    - au dessus : niveau 2
    - paquet du haut : niveau 3.
2. Révéler 4 cartes de chaque niveau afin d’obtenir une grille de 4×3 cartes.

![https://www.regledujeu.fr/wp-content/uploads/splendor-grille.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-grille.jpg)

### **I.b. Tuiles Nobles**

![https://www.regledujeu.fr/wp-content/uploads/nobles-installation.jpg](https://www.regledujeu.fr/wp-content/uploads/nobles-installation.jpg)

1. Mélanger les tuiles Nobles.
2. En révéler n+1 (soit n le nombre de joueurs) et les poser face visible au dessus de la grille.
3. Les autres Nobles ne serviront pas, les ranger dans leur boîte

### **I.c. Jetons**

Le jeu comporte 40 jetons de 6 couleurs. Chaque couleur est en 7 exemplaires, sauf Or (jaune) qui n’existe qu’en 5 exemplaires. Aussi, si vous jouez à 2 ou 3 joueurs, il vous faudra retirer des jetons.

![https://www.regledujeu.fr/wp-content/uploads/splendor-pierres-precieuses.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-pierres-precieuses.jpg)

1. Disposer les Jetons en **6 piles classées par couleur** à la portée des joueurs.
2. A **2 joueurs** : retirer **3 jetons** de chaque couleur sauf Or. (vous jouez donc avec 4 jetons de chaque couleur au lieu de 7, sauf Or).
3. A **3 joueurs** : retirer **2 jetons** de chaque couleur sauf Or (Vous jouez donc avec 5 jetons de chaque couleur sauf Or).

![https://www.regledujeu.fr/wp-content/uploads/splendor-installation.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-installation.jpg)

---

# **II – Tour de Jeu**

### **II.a. Les phases d’un tour**

1. Choix d’UNE **action** parmi les 4 :
    - Prendre **3 jetons** de couleurs différentes
    - Prendre **2 jetons** de la même couleur
    - **Réserver** une carte développement (FS ou FC) ET prendre un joker
    - Acheter une carte développement face visible au centre OU préalablement réservée
2. **Défausser les jetons** si vous en avez plus de 10
3. Visite d’**UN Noble** (obligatoire). Si vous avez cumulé les cartes nécessaires

### **II.b. Les 4 actions**

### **Prendre 3 jetons de couleurs différentes**

![https://www.regledujeu.fr/wp-content/uploads/splendor-3-jetons.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-3-jetons.jpg)

- Vos jetons sont visibles de tous

Rappel : **10 jetons max** dans une main à la fin de votre tour, Or compris.

### **Prendre 2 jetons de même couleur**

![https://www.regledujeu.fr/wp-content/uploads/splendor-2-jetons.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-2-jetons.jpg)

Seulement s’il reste **4 jetons** de cette couleur ou plus.

### **Réserver une carte développement et prendre un joker**

![https://www.regledujeu.fr/wp-content/uploads/splendor-reserver-joker.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-reserver-joker.jpg)

- Au choix :
    - Prenez une carte développement (FV) **Face Visible** au centre de la table.
    - Ou piochez la première carte (FC) **Face Cachée** de l’une des 3 piles sans la dévoiler aux autres joueurs.
- Les cartes réservées sont gardées en main (ou faces cachées sur la table) et ne peuvent pas être défaussées. Le seul moyen de s’en débarrasser étant de l’acheter.
- **Limite de carte réservées** : Vous ne pouvez avoir en main plus de 3 cartes réservées.
- **Joker** : Réserver une carte est le seul moyen d’obtenir un Joker (Or).
- S’il ne reste plus d’Or, vous réservez votre carte, mais ne prenez pas d’Or.

### **Acheter une carte Développement**

![https://www.regledujeu.fr/wp-content/uploads/splendor-carte-developpement.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-carte-developpement.jpg)

- **Payer le prix** : Pour acheter une carte Développement, le joueur doit dépenser le nombre de jetons indiqué sur la carte.
- **Joker** : Un joker (jeton Or) peut remplacer n’importe quelle couleur.
- **Défausser jetons** : Remettre les jetons dépensés au centre de la table.
- **Achat** : Possibilité d’acheter l’une des cartes Développement visibles ou l’une de vos cartes réservées.
- **Tri des Cartes Développement** : Les cartes Développement achetées sont triées en couleurs et disposées en colonnes. Ces dernières sont visibles et superposées de façon à ne faire apparaître que les PV et Bonus.
- **Bonus** : Chaque carte développement possède une pierre représentée dans sa partie supérieure droite. Cette dernière peut être utilisée comme réduction d’achat pour vos futures cartes Développement

### **II.c. Les Nobles**

Les Nobles sont des personnages très sympa qui peuvent s’acquérir gratuitement à la fin de votre tour (même si vous avez acheté une carte ce tour-ci) et qui vous rapporteront des points de victoire. Voici comment procéder :

- **Obligatoire** : À la fin de son tour, chaque joueur vérifie les tuiles noble visibles afin de savoir s’il reçoit la visite de l’un d’entre eux.
    - Un joueur est éligible s’il possède (au moins) la quantité de cartes correspondant au type de bonus indiqués sur la tuile (Ex : si un Noble coûte 3 Bleus, le joueur doit posséder 3 cartes bleues).

![https://www.regledujeu.fr/wp-content/uploads/splendor-nobles.jpg](https://www.regledujeu.fr/wp-content/uploads/splendor-nobles.jpg)

- **Action Bonus** : Il est impossible de refuser la visite d’un noble, qui n’est pas considérée comme une action de jeu.
- **Plusieurs Nobles ?** Si un joueur possède assez de bonus pour recevoir la visite de plusieurs nobles à la fin de son tour, il **choisit** celui qu’il reçoit.
- La tuile obtenue est placée face visible devant le joueur concerné.

# **III – Fin de la partie**

- **Lorsqu’un joueur atteint 15 PV** : terminer le tour (afin que tout le monde ait joué le même nombre de fois).
- **Gagnant** : Le joueur ayant le + de PV l’emporte.
- **En cas d’égalité** : Le joueur ayant acheté le moins de cartes l’emporte.
    - Si les deux joueurs sont à égalité en score et en nombre de cartes achetées, alors c’est partie nulle.