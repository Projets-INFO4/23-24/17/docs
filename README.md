# Fiche de suivi du projet

### April 5, 2024

- Debugg player and playerAction

### April 3, 2024

- Debugg player and playerAction
- Continuation of multiplayer modes

### April 2, 2024

- Debugg player and playerAction
- Continuation of the playerAction class
- Continuation of multiplayer modes

### March 29, 2024

- Information interface linked with code
- Continuation of the playerAction class
- Continuation of multiplayer modes
- Start of report

### March 26, 2024

- Addition of "buy" and "reserved" buttons
- Creation of the playerAction class (for the interface - player link)
- Continuation of multiplayer modes

### March 25, 2024

- Names changed in implementation
- Game and victory rules changed (discussion + start of code)
- Automatic trophy generation on interface
- Continuation of multiplayer modes

### March 22, 2024

- Player actions with jokers done
- Multiplayer mode begins
- Change of logo and game name
- Bug fixes

### March 19, 2024

- End-of-game interface added
- Finalization of player actions (without wildcard actions)
- Bug fixes
- Updated card generations
- Comments changed and added throughout implementation

### March 18, 2024

- Creation of automatic card generation at interface level
- First player actions implemented
- Bug fixes
- Change nobles to trophies on interface

### March 12, 2024

- Debugging the Deck class
- Start of player interface design
- Start of implementation in Player class to attract nobles
- End of implementation of Board and Deck classes
- Creation of links between Game Board, Board and Deck

### March 11, 2024

- Implementation of the Board class (generation of visible cards, hidden cards (Deck), tokens)
- Game interface design
- Player class implementation (card purchase, number of items in hand, number of items without tokens to attract nobles)

### 4 mars 2024

- Implement menu interfaces, room creation and joining a room in Unity.
- Creation of links between interfaces and buttons.
- Follow-up of class development initiated in the last session. Implemented several functions to set up the rules of the game.

### 12 février 2024

- GitHub Desktop installed on all members' computers.
- Start of development of the CardType, Card, Gem, GemType, Deck, Player and Splendor classes.
- Design of all cards, tokens, nobles and game interfaces.

### 29 janvier 2024

- Discussion of how to set up the multiplayer side of our game.
- Creation of use case diagrams for creating and joining a multiplayer game, running a game and ending a game.
- Sequence diagrams for the game's actions.
- Creation of our class diagram.
- Initialize our git repository with an empty Unity project containing the assets we'll be using.

### 15 janvier 2024

- We chose Unity as our tool for developing the game because we understood the advantages of this platform.
- We took the decision to develop a card game called Gemtopia as a mobile game, with an explication of the principles of the game.
- Download of the software: we will use Unity and GitHub Desktop to make the management of Git easier.
- Choice to define and describe the rules of the game in detail in the documentation.
    - Implementation of menu, create room and join room interfaces in Unity.
    - Link interfaces to buttons.
    - Follow-up of class development initiated in last session. Implemented several functions to implement the game rules.
